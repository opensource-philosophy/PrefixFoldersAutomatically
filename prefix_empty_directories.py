#!/usr/bin/env/python3
import os
import sys
import shutil

from PyQt5.QtWidgets import QFileDialog, QApplication # für GUI-Fenster

### Wenn der Ordnerpfad als Kommandozeilenargument angegeben ist, nutze ihn ###
if len(sys.argv) > 1:
    possible_input_folder_path = sys.argv[1]
    if not os.path.isdir(possible_input_folder_path):
        print('Der Ordner, den Sie angegeben haben, existiert nicht!')
        sys.exit()
    else: input_folder_path = possible_input_folder_path
### Ansonsten öffne ein Fenster und frage nach dem Ordner-Pfad ###
else:
    app = QApplication([])
    options = QFileDialog.Options()            # Wähle die Standardoptionen aus
    options |= QFileDialog.DontUseNativeDialog # aber nutze gleiche Oberfläche für jedes Betriebssystem
    input_folder_path = QFileDialog.getExistingDirectory(None, 'Wählen Sie einen Ordner aus')
    if not input_folder_path:
        sys.exit()

def prefix_empty_directories(input_folder_path):
  list_of_directories = [x[0] for x in os.walk(input_folder_path)]
  list_of_empty_directories = [directory for directory in list_of_directories if os.listdir(directory)==[]]
  for empty_directory in list_of_empty_directories:
    super_directory, empty_directory_base = os.path.split(empty_directory)
    if not empty_directory_base.startswith('[L]'):
      new_empty_directory_base = '[L] ' + empty_directory_base
      new_empty_directory = os.path.join(super_directory, new_empty_directory_base)
      rel_empty_directory = os.path.relpath(empty_directory, input_folder_path)
      rel_new_empty_directory = os.path.relpath(new_empty_directory, input_folder_path)
      os.rename(empty_directory, new_empty_directory)
      print(f'\'{rel_empty_directory}\' erfolgreich in \'{rel_new_empty_directory}\' umbenannt!')

prefix_empty_directories(input_folder_path)
app.quit()
